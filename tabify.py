#!/usr/bin/python
import os
import sys
import re
import math

def main():
    afile = open(sys.argv[1], 'r')
    lines = afile.readlines()
    lines = [i.rstrip() for i in lines ]
    for i in lines:
        tabs = 0
        spaces = 0
        temp = ""
        pos = 0
        for c in i:
            if(c == '\t'): tabs += 1
            elif(c == ' '): spaces += 1
            else: break
            pos+=1
        print (int(spaces/4.0)+tabs)*'\t' + i[pos:]
         
if(__name__=="__main__"):
    if(len(sys.argv) < 2):
        sys.exit(2)
    main()
