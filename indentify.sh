#!/bin/bash
BASEDIR=$(dirname $0)
expand -i < "$1" > "$1.tmp.0"
$BASEDIR/indent -kr -nut "$1.tmp.0"
mv "$1" "$1.bak"
python $BASEDIR/tabify.py "$1.tmp.0" > "$1"
rm "$1.tmp.0"
